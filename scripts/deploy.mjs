import 'dotenv/config'
import { createReadStream } from 'fs';

const REFRESH_TOKEN = process.env.REFRESH_TOKEN;
const EXTENSION_ID = process.env.EXTENSION_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const CLIENT_ID = process.env.CLIENT_ID;

const zipPath = "./gitlab-tweaks.zip";

import chromeWebstoreUpload from 'chrome-webstore-upload';

const webStore = chromeWebstoreUpload({
  extensionId: EXTENSION_ID,
  clientId: CLIENT_ID,
  clientSecret: CLIENT_SECRET,
  refreshToken: REFRESH_TOKEN
});

function upload() {
  const zipStream = createReadStream(zipPath);
  webStore
    .uploadExisting(zipStream)
    .then(res => {
      console.log("Successfully uploaded extension");
      console.log("Response:", res);
      if (res.uploadState === "FAILURE" || res.uploadState === "NOT_FOUND") {
        process.exit(1);
      }
    })
    .catch(error => {
      console.log(`Error while uploading extension: ${error}`);
      process.exit(1);
    });
}

upload();
