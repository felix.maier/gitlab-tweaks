import { Requests } from "./requests";

const projectPath = document.location.pathname
  .split("/boards")[0]
  .split("/-")[0]
  .substring(1);
const isGroupBoard = projectPath.startsWith("groups");
let token: string, options: any;
chrome.storage.sync.get(["token", "options"], result => {
  token = result.token || "";
  options = result.options;
});

let iconUrl = document
  .querySelector(".dashboard-shortcuts-issues svg use")
  .getAttribute("href")
  .split("#")[0];

const processCard = async (card: HTMLLIElement) => {
  // get issueId
  const issueId = isGroupBoard
    ? card
        .getElementsByClassName("board-item-path")[0]
        .nextSibling.textContent.trim()
        .substring(1)
    : (<HTMLSpanElement>(
        card.getElementsByClassName("board-card-number")[0]
      )).innerText
        .trim()
        .substring(7);

  const issueProjectPath = isGroupBoard
    ? (<HTMLSpanElement>card.getElementsByClassName("board-item-path")[0])
        .innerText
    : projectPath;

  const req = new Requests(token, issueProjectPath);

  if (options.mrs) {
    // check for merge requests
    req
      .getMergeRequests(issueId)
      .then(response => {
        return response.json();
      })
      .then(mergeRequests => {
        // add info to card
        if (mergeRequests.length) {
          const mrInfo = `<a class="board-card-info card-number board-card-mrs" title="Merge Requests">
            <svg aria-hidden="true" class="s16 ic-git-merge board-card-info-icon">
            <use xlink:href="${iconUrl}#git-merge">
            </use>
            </svg>
            <span class="board-card-info-text">${mergeRequests.length}</span>
            </a>`;
          card
            .getElementsByClassName("board-info-items")[0]
            .insertAdjacentHTML("beforeend", mrInfo);
        }
      });
  }

  if (options.timeStats) {
    // fetch issue data
    req
      .getIssue(issueId)
      .then(issue => {
        let color = "";
        if (issue.time_stats.time_estimate > 0) {
          const ratio =
            issue.time_stats.total_time_spent / issue.time_stats.time_estimate;
          if (ratio > 1.3) {
            color = " text-danger";
          } else if (ratio > 1 && ratio <= 1.3) {
            color = " text-warning";
          }
        }
        // add info to card
        const timeInfo = `<a class="board-card-info card-number board-card-time-stats" title="Time stats">
            <svg aria-hidden="true" class="s16 ic-git-merge board-card-info-icon${color}">
            <use xlink:href="${iconUrl}#timer">
            </use>
            </svg>
            <span class="board-card-info-text">
            <span title="Time estimated">${issue.time_stats
              .human_time_estimate ||
              "-"}</span>/<span title="Time spent" class=${color}>${issue
          .time_stats.human_total_time_spent || "-"}</span></span>
            </a>`;
        card
          .getElementsByClassName("board-info-items")[0]
          .insertAdjacentHTML("beforeend", timeInfo);

        // As an estimate element was added to gitlab by now which shows less data, we'll remove it
        const el = card
          .getElementsByClassName("board-info-items")[0]
          .querySelector('[data-testid="hourglass-icon"]');
        if (el) {
          el.parentElement.parentElement.remove();
        }
      });
  }

  if (options.removeLabel) {
    const labels = card.querySelectorAll(".gl-label-link");

    for (let label of labels) {
      const scoped = label.parentElement.classList.contains('gl-label-scoped')
      if (!scoped) {
        (<HTMLElement>label).style["background-color"] = (<HTMLElement>label.getElementsByClassName('gl-label-text')[0]).style["background-color"]
      }
      label.insertAdjacentHTML(
        "afterend",
        `<button type="button" class="gl-remove-label btn gl-label-close gl-p-0! btn-reset btn-sm gl-button btn-reset-tertiary btn-icon"><svg data-testid="close-icon" aria-hidden="true" class="gl-icon s16"><use href="${iconUrl}#close"></use></svg></button>`
      );
    }

    const removeLabel = async (e: MouseEvent) => {
      e.preventDefault();
      e.stopPropagation();

      const target = <HTMLElement>e.currentTarget;
      const rawLabelText = target.previousElementSibling.textContent.slice(0, -1).trim();
      const labelToRemove = target
        .closest(".gl-label")
        .classList.contains("gl-label-scoped") ? rawLabelText.replace(/\s+/g, "::") : rawLabelText;

      try {
        const req = new Requests(token, issueProjectPath);

        const issue = await req.getIssue(issueId);
        if (!issue.labels.includes(labelToRemove)) {
          alert("Label wasn't found on issue. It was probably removed by someone else in the meantime.");
        }
        const remainingLabels = issue.labels.filter(label => {
          return label !== labelToRemove;
        });
        await req.updateLabels(issueId, remainingLabels);
        target.closest(".gl-label").remove();
      } catch (error) {
        alert(error);
      }
    };

    const buttons = card.querySelectorAll(".gl-remove-label");
    for (let button of buttons) {
      button.addEventListener("click", removeLabel);
    }
  }

  card.classList.add("gl-tweaked");
};

const board = document.getElementById("content-body");

if (board) {
  // When mutation is observed
  const callback = function(mutationsList: MutationRecord[]) {
    //console.log(mutationsList);
    for (let mutation of mutationsList) {
      if (mutation.addedNodes.length) {
        for (let node of mutation.addedNodes) {
          const element = <HTMLLIElement>node;
          if (
            element.classList &&
            element.classList.contains("board-card") &&
            !element.classList.contains("gl-tweaked")
          ) {
            processCard(element);
          }
        }
      }
    }
  };

  // Create a new observer
  const observer = new MutationObserver(callback);

  // Start observing
  observer.observe(board, {
    childList: true,
    subtree: true
  });
}
