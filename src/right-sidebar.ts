import { Requests } from "./requests";

let token: string, options: any;
chrome.storage.sync.get(["token", "options"], result => {
  token = result.token || "";
  options = result.options;
});

let iconUrl = document
  .querySelector(".dashboard-shortcuts-issues svg use")
  .getAttribute("href")
  .split("#")[0];

const board = document.getElementById("content-body");

if (board) {
  // When mutation is observed
  const callback = function(mutationsList: MutationRecord[]) {
    //console.log(mutationsList);
    for (var mutation of mutationsList) {
      if (mutation.addedNodes.length) {
        for (let node of mutation.addedNodes) {
          const parentNode = <HTMLElement>node.parentNode;
          if (
            parentNode.classList.contains("issuable-show-labels") &&
            !parentNode.classList.contains("gl-tweaked")
          ) {
            if (!options.removeLabel) return;

            const labels = (<HTMLElement>node).querySelectorAll(".gl-label-link");

            for (let label of labels) {
              label.insertAdjacentHTML(
                "afterend",
                `<button type="button" class="gl-label-close gl-remove-label"><svg data-testid="close-icon" aria-hidden="true" class="gl-icon s16"><use href="${iconUrl}#close"></use></svg> <span class="gl-sr-only">Remove label</span></button>`
              );
            }

            const removeLabel = async (e: MouseEvent) => {
              e.preventDefault();
              const target = <HTMLButtonElement>e.currentTarget;
              const issueId = (<HTMLSpanElement>(
                target
                  .closest(".issuable-sidebar")
                  .querySelector(".issuable-header-text span")
              )).innerText
                .trim()
                .substring(1);

              const button: HTMLButtonElement = target
                .closest(".block.labels")
                .querySelector(".js-label-select");

              const rawLabelText = target.previousElementSibling.textContent.slice(0, -1).trim();
              const labelToRemove = target
                .closest(".gl-label")
                .classList.contains("gl-label-scoped") ? rawLabelText.replace(/\s+/g, "::") : rawLabelText;

              try {
                const req = new Requests(token, button.dataset.projectId);

                const issue = await req.getIssue(issueId);

                if (!issue.labels.includes(labelToRemove)) {
                  alert("Label wasn't found on issue. It was probably removed by someone else in the meantime.");
                }
                const remainingLabels = issue.labels.filter(
                  label => {
                    return label !== labelToRemove;
                  }
                );

                req.updateLabels(issueId, remainingLabels);
                target.parentElement.remove();
              } catch (error) {
                alert(error);
              }
            };

            const buttons = (<HTMLElement>node).querySelectorAll(
              ".gl-remove-label"
            );
            for (let button of buttons) {
              button.addEventListener("click", removeLabel);
            }
          }
        }
      }
    }
  };

  // Create a new observer
  const observer = new MutationObserver(callback);

  // Start observing
  observer.observe(board, {
    childList: true,
    subtree: true
  });
}
