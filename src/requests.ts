import { Gitlab } from "gitlab";

export class Requests {
  token: string;
  apiUrl: string;
  projectPath: string;
  api;

  constructor(token: string, projectPath: string) {
    this.token = token;
    this.apiUrl = "https://gitlab.com/api/v4";
    this.projectPath = projectPath;

    this.api = new Gitlab({token})
  }

  getIssue(issueId: string) {
    return this.api.Issues.show(this.projectPath, issueId);
  }

  getMergeRequests(issueId: string) {
    return fetch(
      `${this.apiUrl}/projects/${encodeURIComponent(
        this.projectPath
      )}/issues/${issueId}/related_merge_requests?private_token=${this.token}`
    );
  }

  getEpicIssues(epicId: string) {
    return this.api.EpicIssues.all(this.projectPath, epicId);
  }

  getGroupLabels(group: string) {
    return this.api.GroupLabels.all(group);
  }

  updateLabels(issueId: string, newLabels: string[]): Promise<any> {
    return this.api.Issues.edit(this.projectPath, issueId, {labels: newLabels});
  }
}
