const $form = document.querySelector("form");
const $message = document.querySelector(".message");

function showMessage(message: string, duration?: number): void {
  $message.textContent = message;

  if (duration) {
    setTimeout(() => {
      $message.textContent = "";
    }, duration);
  }
}

const optionKeys = ['mrs', 'timeStats', 'removeLabel', 'showWeightSum', 'showTimeStats', 'showEpicIssueListLink']

document.body.onload = () => {
  chrome.storage.sync.get(["token", "options"], result => {
    result.options = result.options || { mrs: true, timeStats: true };
    $form.token.value = result.token || false;
    for (const option of optionKeys) {
      $form[option].checked = result.options[option];
    }
  });
  const manifest = chrome.runtime.getManifest();
  showMessage('Version: ' + manifest.version);
};

$form.addEventListener("submit", async e => {
  e.preventDefault();

  const token = $form.token.value;
  const options = {};
  for (const option of optionKeys) {
    options[option] = $form[option].checked;
  }

  showMessage("Saving...");
  $form.save.disabled = true;
  const response = await fetch(
    `https://gitlab.com/api/v4/user?private_token=${token}`
  );
  const user = await response.json();
  chrome.storage.sync.set({ token, options }, () => {
    showMessage(`Hi ${user.name}. Your token was saved`, 1000);
    $form.save.disabled = false;
  });
});
