import { Requests } from "./requests";
console.log("asdfasdf");
let token: string, options: any;
chrome.storage.sync.get(["token", "options"], result => {
  token = result.token || "";
  options = result.options;
});

const [projectPath, epicId] = document.location.pathname
  .substring(1)
  .split("/-/epics/");
const group = projectPath.split("groups/")[1];
console.log(group);
const req = new Requests(token, group);

const board = document.querySelector(".js-epic-container");

let iconUrl = document
  .querySelector(".dashboard-shortcuts-issues svg use")
  .getAttribute("href")
  .split("#")[0];

function getIssuesCallback(result) {
  const [issues] = result;
  if (options.showWeightSum || options.showTimeStats) {
    let markup = "";
    const issuesHeader = document.querySelector('.card-header .issue-count-badge')

    if (options.showWeightSum) {
      // add sum of weight to issue header
      const weight = {
        total: 0,
        open: 0,
        closed: 0
      }

      issues.map((issue) => {
        weight.total += issue.weight;
        issue.state === 'closed' ? weight.closed += issue.weight : weight.open += issue.weight;
      })

      markup +=
        `<div title="Weight (open / closed / total)" style="white-space: break-spaces;font-weight: bold;" class="weight-sum-badge gl-display-inline-flex text-secondary p-0 pr-3"><span class="d-inline-flex align-items-center">
      <svg aria-hidden="true" class="text-secondary mr-1 s16 ic-issues"><use xlink:href="${iconUrl}#weight">
      </use></svg><span class="gl-text-green-500">${weight.open}</span> / <span class="gl-text-blue-500">${weight.closed}</span> / ${weight.total} </span></div>`;
    }

    if (options.showTimeStats) {
      // add time stats to issue header
      const time = {
        estimate: 0,
        spent: 0
      }

      issues.map((issue) => {
        time.estimate += issue.time_stats.time_estimate;
        time.spent += issue.time_stats.total_time_spent
      })

      time.estimate = time.estimate / 3600
      time.spent = time.spent / 3600

      markup +=
        `<div title="Estimated / Time spent" style="white-space: break-spaces;font-weight: bold;" class="time-stats-badge gl-display-inline-flex text-secondary p-0 pr-3"><span class="d-inline-flex align-items-center">
<svg aria-hidden="true" class="text-secondary mr-1 s16 ic-issues"><use xlink:href="${iconUrl}#clock">
</use></svg>${time.estimate.toFixed(1)}h / ${time.spent.toFixed(1)}h</div>`;
    }

    issuesHeader.insertAdjacentHTML('afterend', markup);
  }
}

if (board) {
  // When mutation is observed
  const callback = function (mutationsList: MutationRecord[]) {
    for (var mutation of mutationsList) {
      if (mutation.addedNodes.length) {
        for (let node of mutation.addedNodes) {
          if (
            (<Element>node).classList?.contains("card-header") &&
            !(<Element>node).classList?.contains("gl-tweaked") && (options.showEpicLabels || options.showWeightSum || options.showEpicIssueListLink)
          ) {
            Promise.all([req.getEpicIssues(epicId)]).then(getIssuesCallback)

            if (options.showEpicIssueListLink) {
              const epicIId = (<HTMLElement>document.getElementsByClassName('related-items-list')[0]).dataset.parentId.split('Epic/')[1]
              const listUrl = document.location.pathname.split('epics/')[0] + 'issues?epic_id=' + epicIId;
              const listButton = `<div class="d-inline-flex flex-column flex-sm-row js-button-container"><button type="button" class="btn btn-default btn-todo float-right"><a href="${listUrl}" class="issuable-todo-inner">Show in issue list</a></button></div>`;
              const cardHeader = document.querySelector('.card-header');
              cardHeader.children[0].insertAdjacentHTML('afterend', listButton)
            }
          }
        }
      }
    }
  };

  // Create a new observer
  const observer = new MutationObserver(callback);

  // Start observing
  observer.observe(board, {
    childList: true,
    subtree: true
  });
}
