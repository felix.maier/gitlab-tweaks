# Gitlab Tweaks

This project provides a simple chrome extension which improves some of Gitlabs shortcomings.

## Setup

To be able to query data from Gitlab the extension needs a personal access token. You can insert it through the extension's popup.

## Features as of now

All features are optional and can be de/activated.

### Boards

- add estimated and spent time stats on issue cards
- add count of related merge requests to boards

### Epics

- add labels to issues on the epic view
- add a link to the normal issue list, filtered by the currently viewed epic
- add sum of weight of issues assigned to an epic (plus sum of open/closed issue weight)

## Why create a browser extension instead of directly contributing to gitlab?

Although I'd like to do this in the future, atm I just want to get things done quickly, without the need to get into another very complex project :)

# Development

1. run `yarn && yarn dev`
1. go to `chrome://extensions/`
1. click `load unpacked` and point it to your local `gitlab-tweaks/dist` folder
1. optional: install [Extension Reloader](https://chrome.google.com/webstore/detail/extensions-reloader/fimgfedafeadlieiabdeeaodndnlbhid)

Note if you add new files or make changes to the manifest, Extension reloader is not sufficient and you need to reload the extension completely from `chrome://extensions/`

# Publishing a new package

1. give changelog a once over
1. bump version in `package.json` and `src/manifest.json`
1. create the according git tag
1. push the tag -> the package is automatically created and uploaded by gitlab ci
1. Adjust chrome webstore listing with changelog/screenshots/etc

![Gitlab Tweaks Logo](src/icons/logo16.png "Gitlab Tweaks Logo") Logo created by https://gitlab.com/hannadeeds
