# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.9] - 2022-03-09

# Added

- Epic detail view: show time tracking stats on issue list header

### Changed

- Moved to parcel2
- Epic Detail: Removed obsolete "show issue labels" option

### Fixed

- Issue Board: fix estimate selector, so that it's removed if timeStats are active
- Issue Board: adapt to new markup structure and sub groups
- Issue Board: fix icon url in right sidebar
- Epic Detail: fix icon url and adapt to sub groups
- Options popup: fix links to access token page
- Issue Board: fix "remove label" button styling

## [0.8] - 2020-12-05

### Added

- Epic Detail: show link button which leads to the issue list filtered by the shown epic

### Fixed

- Issue Detail: remove duplicate "Remove label button" as gitlab now finally has it's own one
- Issue Boards: adjust styling and adapt to new markup structure for "remove label" on issue cards
- Issue Boards: adjust styling and adapt to new markup structure for "remove label" in right sidebar

## [0.7] - 2020-05-11

### Added

- Epic Detail: add open/closed weight
- Epic Detail: render smaller labels
- Epic Detail: render scoped labels properly styled

### Fixed

- Epic Detail: properly position weight in header
- Boards: adjust to updated markup structure and use proper icon for "remove label"

## [0.6] - 2020-05-11

### Added

- Epic Detail: show sum of weight of issues
- Extension description: note that only api scope is needed for gitlab token

### Fixed

- Epic Detail: always show labels in a second row for consistent layout
- Epic Detail: add fallback if label data can't be found
- Webstore upload through CI works again
- Fix broken build

## [0.5] - 2020-03-15

### Added

- Allow one click removal of labels from issue detail view sidebar
- Show labels for issues on epic detail view

### Fixed

- Adjust projectPath recognition to new url schema
- Adjust remove label function to new labels markup and scoped labels for all views

## [0.4] - 2019-05-12

### Added

- Use parcel/babel so import statements can be used and files can be splitted
- Allow direct removal of labels from board cards
- Time stats element will now turn orange if the spent/estimated ratio is greater than 1 and red if greater than 1.3
- Use new logo created by hanna
- Migrate to typescript

### Updated

- refactored sidebar code into own file for reusability

### Fixed

- Fix regex so scripts are also triggered for unnamed boards (for projects that may only have a single board)
- Remove duplicated "time estimate" data element instead of trying to weirdly merge functionalities

## [0.3]

### Added

- Allow removing labels from the board sidebar

### Fixed

- Use dynamic svg url for icons

## [0.2]

### Added

- Show time stats on issue cards
- Allow en/disabling of features

### Changed

- Use MutationObserver for adding features instead of a timeout

## [0.1] - 2019-01-06

### Added

- Show amount of merge requests on issue cards
